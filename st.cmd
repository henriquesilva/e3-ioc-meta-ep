require essioc
require epickup
require iocmetadata

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("R_TMP100",             ":Temp")
epicsEnvSet("R_M24M02",             ":Eeprom")
epicsEnvSet("R_TCA9555",            ":IOExp")
epicsEnvSet("R_LTC2991",            ":VMon")
epicsEnvSet("R_AD527X",             ":Res")

epicsEnvSet("PREFIX",               "{{P}}{{R}}")
epicsEnvSet("DEVICE_IP",            "{{IP}}")
epicsEnvSet("I2C_COMM_PORT",        "AK_I2C_COMM")

# Leakage current limits
epicsEnvSet("LEAK_HIHI",            "100")
epicsEnvSet("LEAK_HIGH",            "90")
epicsEnvSet("LEAK_LOW",             "0")
epicsEnvSet("LEAK_LOLO",            "0")

# Threshold current limits
epicsEnvSet("THRS_HIHI",            "100")
epicsEnvSet("THRS_HIGH",            "90")
epicsEnvSet("THRS_LOW",             "0")
epicsEnvSet("THRS_LOLO",            "0")

iocshLoad("$(epickup_DIR)/e-pickup.iocsh", "PREFIX=$(PREFIX), I2C_COMM_PORT=$(I2C_COMM_PORT), DEVICE_IP=$(DEVICE_IP), LEAK_HIHI=$(LEAK_HIHI), LEAK_HIGH=$(LEAK_HIGH), LEAK_LOW=$(LEAK_LOW), LEAK_LOLO=$(LEAK_LOLO), THRS_HIHI=$(THRS_HIHI), THRS_HIGH=$(THRS_HIGH), THRS_LOW=$(THRS_LOW), THRS_LOLO=$(THRS_LOLO)")

pvlistFromInfo("ARCHIVE_THIS", "$(IOCNAME):ArchiverList")

iocInit
